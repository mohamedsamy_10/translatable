<?php

namespace Mosamy\Translatable;

trait Translatable
{

    public static function bootTranslatable(){
      static::deleting(function ($model) {
        $model->translations()->delete();
      });
    }

    public function initializeTranslatable(){
      array_push($this->with, 'translations');
    }

    public function getAttribute($key){
      $attribute = parent::getAttribute($key);
      return $attribute ?? $this->translate($key);
    }

    public function scopeWhereTranslation($query, $keyword, $attributes = [], $locale = [], $like = true){

      if(!$attributes && defined('TranslatableAttributes')) $attributes = self::TranslatableAttributes;
      if(!$locale) $locale = [app()->currentLocale()];

      $query->when($keyword, function($query) use($keyword, $attributes, $locale, $like){
        $query->whereHas('translations', function($translation) use($keyword, $attributes, $locale, $like){
          $translation
          ->when($locale, fn($q) => $q->whereIn('locale', $locale))
          ->when($attributes, fn($q) => $q->whereIn('attribute', $attributes))
          ->when($like,
            fn($q) => $q->where('body', 'like', '%'.$keyword.'%'),
            fn($q) => $q->where('body', $keyword)
          );
        });
      });
    }

    public function scopeHasTranslation($query, $locale = null)
    {
        return $query->whereHas('translations', function ($translation) use ($locale) {
            $translation->when($locale, fn ($t) => $t->whereLocale($locale));
        });
    }

    public function scopeHasCurrentTranslation($query)
    {
        return $query->hasTranslation(app()->currentLocale());
    }
    
    public function scopeOrderByTranslation($query, $attribute, $sort = 'asc', $locale = null){

        $table = with(new static)->getTable();
        $model = class_basename(with(new static));
        $locale ??= app()->currentLocale();

        $query->addSelect(['orderByTranslation' => Translations::select('body')
          ->whereColumn('translatable_id', $table.'.id')
          ->where('translatable_type', 'App\Models\\'.$model)
          ->where('locale', $locale)
          ->where('attribute', $attribute)
        ])->orderBy('orderByTranslation', $sort);
    }

    public function translations(){
        return $this->morphMany(Translations::class, 'Translatable', 'translatable_type', 'translatable_id');
    }

    public function getTranslationsListAttribute(){
      return isset($this->translations) ?
      $this->translations->groupBy('locale')->map->pluck('body','attribute')
      : [];
    }

    public function translate($attribute, $locale = null){
      $locale ??= app()->currentLocale();
      return isset($this->translations) ?
      $this->translations->where('locale', $locale)->where('attribute', $attribute)->first()?->body
      : null;
    }
	
    public function scopeTranslateOnly($query, $attributes){
        if(!is_array($attributes)) {
            $attributes = [$attributes];
        }
        return $query->with(['translations' => fn ($t) => $t->whereIn('attribute', $attributes)]);
    }

    public function createTranslations($translations){
      $translation_data = [];
      foreach ($translations as $locale => $translation) {
        foreach ($translation as $attribute => $body) {
          if($body){
            array_push($translation_data,[
              'locale' => $locale,
              'attribute' => $attribute,
              'body' => $body,
            ]);
          }
        }
      }
      $this->translations()->delete();
      $this->translations()->createMany($translation_data);
    }
}

 ?>
