<?php

namespace Mosamy\Translatable;

use Illuminate\Database\Eloquent\Model;

class Translations extends Model
{

    protected $guarded = [];
    protected $hidden = ['translatable_type', 'translatable_id'];
    public $timestamps = false;

    public function Translatable(){
        return $this->morphTo();
    }
}
