<?php

namespace Mosamy\Translatable\Rules;

use Illuminate\Contracts\Validation\Rule;

class Unique implements Rule
{

  protected $model;
  protected $attribute;
  protected $ignore_id;
  protected $locale = [];

  public function __construct($model, $attribute = null){
    $this->model = $model;
    $this->attribute = $attribute;
  }

  public function setLocale(array $locale){
    $this->locale = $locale;
    return $this;
  }

  public function ignore($id){
    $this->ignore_id = $id;
    return $this;
  }

  public function message(): string
  {
      return ':attribute حقل موجود بالفعل';
  }

  public function passes($attribute, $value): bool
  {
      $field = $this->attribute ?: explode('.', $attribute)[2];
      $locale = $this->locale ?: [explode('.', $attribute)[1]];

      return !$this->model
      ->when($this->ignore_id, fn($q) => $q->where('id', '!=', $this->ignore_id))
      ->whereTranslation($value, [$field], $locale, false)
      ->exists();
  }

}
